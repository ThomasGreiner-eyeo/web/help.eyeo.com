title=Removing a browser language
description=Remove a previously added language from Adblock Browser for Android.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Languages**.
4. Tap the **menu** icon next to the language that you want to remove and select **Remove**.
<br>The language is removed.
