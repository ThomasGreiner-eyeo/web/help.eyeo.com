title=Adding a filter list
description=How to add filter lists to Adblock Browser
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

Adblock Browser requires filter lists to block ads. A default filter list based on your browser's language settings is automatically activated when you install Adblock Browser. It blocks ads from the most popular (often English language-based) websites. However, it does not block ads on less popular national websites. For example, if you live in Germany you should subscribe to the national German filter list. This filter list is detected as "EasyList Germany + EasyList".

<aside class="alert info" markdown="1">
**Tip**: As a recommendation, you should avoid adding too many filter lists to Adblock Browser. Adding too many filter lists slows down the ad blocker and, therefore, your browsing.
</aside>

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **More blocking options**.
5. Enter the URL of the filter list in the space provided.
6. Tap the "+" icon.

![Add a filter in Adblock Browser](/src/img/gif/add-filter-abb.gif)
