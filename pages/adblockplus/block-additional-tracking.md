title=Block additional tracking
description=Use Adblock Plus to prevent entities from tracking your activity across the websites you visit.
template=article
product_id=abp
category=Customization & Settings

Entities may track your activity across the websites that you visit. To prevent this, turn on Adblock Plus's Block additional tracking feature.

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block additional tracking**.
4. Close the tab.

![ABP_BlockTrack_Chrome](dist/img/gif/ABP_BlockTrack_Chrome.gif)
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block additional tracking**.
4. Close the tab.

![ABP_BlockTrack_edge](dist/img/gif/ABP_BlockTrack_Edge.gif)
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block additional tracking**.
4. Close the tab.

![ABP_BlockTrack_Firefox](dist/img/gif/ABP_BlockTrack_Firefox.gif)
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block additional tracking**.
4. Close the tab.

![ABP_BlockTrack_Opera](dist/img/gif/ABP_BlockTrack_Opera.gif)
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. From the *General* tab, locate the *Privacy &amp; Security* section.
3. Select the check box labeled **Block additional tracking**.
4. Close the tab.

![ABP_BlockTrack_Yandex](dist/img/gif/ABP_BlockTrack_Yandex.gif)
</section>
