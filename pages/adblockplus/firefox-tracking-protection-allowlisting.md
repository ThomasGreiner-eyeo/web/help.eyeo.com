title=Firefox's Tracking Protection and allowlisting
description=When using Firefox, certain websites with anti-ad block messages do not recognize that Adblock Plus (ABP) is turned off.
template=article
product_id=abp
category=Troubleshooting & Reporting
hide_browser_selector=true

## Issues with Firefox's Tracking Protection feature and allowlisted websites

Some Adblock Plus for Firefox users who have Firefox's Tracking Protection feature turned on are experiencing an issue where certain websites with anti-ad block messages do not recognize that Adblock Plus is turned off.

**Scenario**

1. A user visits a website with an anti-ad block message that requires them to turn off Adblock Plus in order to browse the site.
2. The user either already has the website allowlisted OR clicks the Adblock Plus icon in the toolbar and temporarily turns off Adblock Plus on the site.
3. The website still displays the anti-ad block message.

**Workarounds**

1. Click the **Information** icon to the left of the Firefox address bar.
![Firefox-information-icon](/src/img/png/firefox-information-icon.png)
2. Select **Disable protection for this site**.

OR

Turn off Firefox's Tracking Protection feature:

1. Click the **Firefox menu** icon and select **Preferences**.
2. Select **Privacy & Security** from the left menu.
3. Scroll to the *Tracking Protection* feature and select **Never**.
4. Close the tab.

<aside class="alert info" markdown="1">
**Tip**: Other extensions (those that blocks scripts or requests) may also cause this issue.
</aside>