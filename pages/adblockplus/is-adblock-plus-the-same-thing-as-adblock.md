title=Is Adblock Plus the same thing as AdBlock?
description=Adblock Plus is not the same thing as AdBlock.
template=article
product_id=abp
category=Popular Topics
popular=true
hide_browser_selector=true

In short — no. Both are ad blockers, but they are separate projects. Adblock Plus is a version of the original "ad-blocking" project while AdBlock originated in 2009 for Google Chrome.

### Want to confirm which  ad blocker you are using? 

See logos below:

![Adblock Plus logo](/src/img/png/ABP-logo.jpeg)

Adblock Plus can be downloaded here [https://adblockplus.org/en/download](https://adblockplus.org/download)

![AdBlock logo](/src/img/png/AdBlock-logo.jpeg)

AdBlock  can be downloaded here [https://getadblock.com/](https://getadblock.com/)

### Want a longer story... happy ending? 

Adblock Plus (ABP) was created as a Firefox add on way back in the day. When Google Chrome came along, Adblock Plus didn't initially support the new browser. AdBlock was created as a Google Chrome add on. Later on, the Adblock Plus team decided to support Chrome as well. Over time, both AdBlock and ABP added support for additional browsers and platforms. eyeo, the company behind Adblock Plus eventually acquired AdBlock. Though the two products remain separate, the teams will collaborate on future projects. We're happy to introduce the [AdBlock VPN](https://getadblock.com/en/beta/vpn/) as the first product from the combined team.
