title=How to check your browser security settings
description=Learn how to check your browser security settings that may interfere with allowlisting websites.
template=article
product_id=abp
category=Troubleshooting & Reporting

Some browsers have pre-installed security settings that may interfere with your allowlisting. Here's how to check those settings.

<section class="platform-chrome" markdown="1">
## Chrome

1. Click the 3 dots in the right corner of the browser
2. Choose **Settings**
3. Scroll down to **Privacy and security** ( you can clear cache and cookies in this section)
4. Click on **Site settings**
5. Check for pop up blocking and Javascript blocking as these may interfere with allowlisting. 
6. If the item is showing as **Blocked**, toggle to **Allow**
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. Click the **...** in the right corner of the browser
2. Choose **Privacy & Security**

Here you can toggle pop up blockers and additional tracking protection to off.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

Firefox's tracking protection is often the culprit for interfering with allowlisting in ABP

1. Click on the Shield icon to the left of the address bar.
2. If it shows **"Enhanced tracking protection is enabled for this site"**, toggle off.

You can also click on **Protection settings**  to further customize these settings.
</section>

<section class="platform-msie" markdown="1">
### Internet Explorer

Reference the link below to learn how to check security settings in Internet Explorer:

[Click Here](https://support.microsoft.com/en-us/topic/change-security-and-privacy-settings-for-internet-explorer-11-9528b011-664c-b771-d757-43a2b78b2afe)

</section>

<section class="platform-opera" markdown="1">
## Opera

1. Click the icon in the right corner of the browser
2. Scroll down to the **Privacy & security** section

Here you can toggle pop up blockers and tracking protection to off.
</section>

<section class="platform-safari" markdown="1">
### Safari

1. Click on **Safari ** in the left corner of the browser
2. Click on **Preferences**
3. Click **Security**
4. Make sure Javascript is enabled
5. You can disable tracking protection in the **Privacy tab**
</section>

<section class="platform-samsungBrowser" markdown="1">
## Adblock Plus for Samsung Internet

1. Click the setting menu icon in the bottom left corner of the page
2. Choose **Privacy & Security**

Here you can toggle smart anti-tracking or **Block unwanted pages** (usually redirects and pop ups) to off.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. Security icons are displayed in the SmartBox to the right and left of the page address.
2. Click the icon to open the Protect panel.

On the panel, you will see security details. You can also change security settings there.
</section>