title=Remove a website from the allowlist
description=No longer want to allow ads on websites that you previously allowlisted? Remove the websites from your Adblock Plus allowlist.
template=article
product_id=abp
category=Customization & Settings
include_heading_level=h2

Follow the steps below if you added a website to the Adblock Plus allowlist, but now want to remove it.

<? include adblockplus/removeallowlist ?>
