title=What is adware and what can I do about it?
description=What are adware and malware and how do I get rid of them?
template=article
product_id=abp
hide_browser_selector=1
category=Troubleshooting & Reporting
popular=false

<head>
    <style>
        .icon-header em {
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            line-height: 20px;
            margin-right: 7px;
            padding: 7px 0px;
            width: 34px;
            border-radius: 4px;
            color: #fff;
            background-color: #53b044;
            font-weight: bold;
            font-style: normal;
            font-size: 20px;      
        }
        .narrow-list-block {
            display: inline-block;
            width: 250px;
            margin: 0px 35px 0px 0px;
            vertical-align: top;
        }
        .narrow-list-block a {
            display: block;
        }
        .narrow-list-block p {
            margin-top: 0px;
        }
        .tips-list {
            padding-left: 0px !important;
        }
        li.icon {
            padding: 20px 0px 0px 40px;
            overflow: visible;
            position: relative;
            list-style: none;
        }
        li.icon::before {
            content: " ";
            position: absolute;
            left: 0px;
            right: auto;
            top: 22px;
            height: 20px;
            width: 40px;
        }
        li.icon.check::before {
            background: url(/src/img/png/check.png) no-repeat center top;
        }
        li.icon.cross::before {
            background: url(/src/img/png/cross.png) no-repeat center top;
        }
        li.icon.cross strong {
            color: #e11a2c;
        }
    </style>
</head>

There are a few reasons why you might still see ads after installing Adblock Plus. Before checking for adware, we recommend that you try the first five options listed in the [I still see ads](adblockplus/i-still-see-ads) article. If you still see ads, your computer might be infected with adware.

## What is adware? {: #what-is-adware }

"Adware" is a blanket term for software that displays unwanted advertisements and / or other annoyances on your computer.

There are two types of adware:

Non-malicious adware 
:   Non-malicious adware is software that hides on your computer and serves unwanted advertisements when you browse the web. The purpose of non-malicious adware is to generate revenue for its developer. Oftentimes users have no knowledge of installing non-malicious adware because it's typically bundled in software that they've knowingly downloaded.

Malicious adware (malware)
:   Malware is usually obtained during routine web browsing, oftentimes when a user downloads a file from an unreliable source. The purpose of malware is to infiltrate your computer's operating system in order to collect personal data like passwords, credit card numbers, and pictures. If your computer is riddled with incessant pop-ups and / or windows that you can't close, there's a good possibility that you've obtained malware.

## Why doesn't Adblock Plus block ads that contain adware? {: #block-adware }

Adblock Plus only blocks ads that are delivered from websites and / or their advertising partners. 

The best way to stop adware is to remove the software that displays it.

## How can I safely remove adware on my computer? {: #remove-adware }

### *1* Remove unwanted software {: #remove-unwanted-software .icon-header }

We recommend running automated checks for adware with one of the programs listed below, which also allow you to remove any found unwanted software. All of the programs are free (for at least a trial period) with full functionality and safe to install alongside your antivirus solution.

For both Windows and macOS, we recommend [Malwarebytes](https://www.malwarebytes.com/). Other options for Windows include [HitmanPro](http://www.surfright.nl/hitmanpro) and [AdwCleaner](https://toolslib.net/downloads/viewdownload/1-adwcleaner/).

#### Additional resources for removing unwanted software

[Windows](https://malwaretips.com/blogs/malware-removal-guide-for-windows/)
<br>[macOS](https://support.apple.com/en-us/HT203987)

### *2* Reset your browser settings {: #reset-browser-settings .icon-header }

After removing unwanted software, we recommend that you restore your browser settings to their original state. Follow these guides to do this manually:

<div class="narrow-list-block" markdown="1">

[Chrome](https://support.google.com/chrome/answer/3296214)
[Edge](https://support.microsoft.com/en-us/help/4023560/windows-10-what-to-do-if-microsoft-edge-not-working)
[Firefox](https://support.mozilla.org/kb/reset-firefox-easily-fix-most-problems)

</div>

<div class="narrow-list-block" markdown="1">

[Opera](http://winaero.com/blog/how-to-reset-opera-browser-settings-to-their-defaults/)
[Safari](https://support.intego.com/hc/en-us/articles/115003863692-How-To-Reset-Your-Safari-Web-Browser)
[Yandex Browser](https://help.yandex.com/newbrowser/faq/faq-settings.xml#reset)

</div>

## How can I prevent my computer from getting infected with adware in the future? {: #future-prevention }

<ul class="tips-list">
    <li class="icon check">
        <strong>Always use official channels to download your browser:</strong>
        <ul>
            <li><a href="https://www.google.com/chrome/browser/desktop/">Chrome</a></li>
            <li><a href="https://www.microsoft.com/en-us/windows/microsoft-edge">Edge</a></li>
            <li><a href="http://www.mozilla.org/firefox">Firefox</a></li>
            <li><a href="http://www.opera.com/">Opera</a></li>
            <li><a href="https://www.apple.com/safari/">Safari</a></li>
            <li><a href="https://browser.yandex.com/">Yandex Browser</a></li>
        </ul>   
    </li>
    <li class="icon check">
        <strong>Install Adblock Plus.</strong> Adblock Plus helps block and hide ads that trick you into installing potentially unwanted programs. Get Adblock Plus from <a href="https://adblockplus.org/">https://adblockplus.org/</a>.
    </li>
    <li class="icon check">
        <strong>Keep your computer's operating system and other software up-to-date.</strong>
    </li>
    <li class="icon cross">
        <strong>Don't click inside misleading pop-ups.</strong> Many malicious websites try to install malware on your system by making images look like pop-up windows, or by displaying an animation of the website scanning your computer.
    </li>
    <li class="icon cross">
        <strong>Don't install untrusted software.</strong> Some websites offer you software to accelerate your browser, to help you search the web, or to add toolbars that do things your browser already does. Some unwanted programs also come bundled in software packages. Usually, these programs gather information on your browsing behavior that serve only the people who designed them. Make sure you only install add-ons, extensions, and plug-ins from your browser’s web store and that you uncheck unwanted programs in software installation wizards.
    </li>
</ul>

