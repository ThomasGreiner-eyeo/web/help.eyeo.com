const { src, dest, parallel } = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');

function processJS() {
  return src('./static/src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('./static/dist/js'));
}

function processCSS() {
  return src('./static/src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename('main.min.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('./static/dist/css'));
}

function processImg() {
  return src('./static/src/img/**')
    .pipe(imagemin([
      imagemin.svgo({
        plugins: [
          {removeDimensions: false},
          {removeXMLNS: false},
          {cleanupIDs: true}
        ]
      })
    ]))
    .pipe(dest('./static/dist/img'));
}

exports.default = parallel(processCSS, processJS, processImg);