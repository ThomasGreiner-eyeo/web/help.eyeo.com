1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
<br>The *Adblock Plus Settings* tab opens.
2. Select the **Allowlisted websites** tab.
3. If a website that you do not want to view ads on is listed, click the **Trash** icon next to the website.
4. Close the tab.