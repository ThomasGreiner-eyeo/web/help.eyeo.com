1. Open the Adblock Plus for iOS app.
2. Tap the **Tools** icon.
3. Tap **Acceptable Ads**.
4. Turn off **Allow some nonintrusive ads**.
5. Close the app.
